import './App.css';
import './fonts.css';

function App() {
  return (
    <div className="App">
      <h1 className="app-header">Список задач</h1>
      <div className="tasklist rainbow-border">
        <a  href="#" className="tasklist-item">Все задачи</a>
        <a href="#" className="tasklist-item">Не выполнено</a>
        <a href="#" className="tasklist-item">Выполнено</a>
      </div>
      <div className="input">
        <input className="input-item rainbow-border" placeholder="Введи свою новую цель!"></input>
        <button className="input-button rainbow-border"> Создать новую цель!</button>
      </div>
      <div className="checktask rainbow-border">
        <input type="checkbox" className="checkbox-task"></input>
        <p className="checktask-text">задача 1</p>
      </div>
      <div className="checktask rainbow-border">
        <input type="checkbox" className="checkbox-task"></input>
        <p className="checktask-text">задача 2</p>
      </div>
    </div>
  );
}

export default App;
